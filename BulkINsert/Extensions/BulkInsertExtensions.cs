﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.ExceptionServices;
using BulkInsert.Infrastructure;
using System.Data.EntityClient;
using System.Data;

namespace BulkInsert.Extensions
{
	public static class BulkInsertExtensions
	{
		// TODO: Add transaction support

		public static void BulkInsert<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source)
			where TEntity: class
			where TContext: DbContext
		{
			context.BulkInsert(source, BulkInsertOptions.Create());
		}
		public static void BulkInsert<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source, int batchSize)
			where TEntity : class
			where TContext : DbContext
		{
			context.BulkInsert(source, BulkInsertOptions.Create(batchSize: batchSize));
		}
		public static void BulkInsert<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source, int batchSize, int timeout)
			where TEntity : class
			where TContext : DbContext
		{
			context.BulkInsert(source, BulkInsertOptions.Create(timeout, batchSize));
		}

		public static Task BulkInsertAsync<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source)
			where TEntity : class
			where TContext : DbContext
		{
			return context.BulkInsertAsync(source, BulkInsertOptions.Create());
		}
		public static Task BulkInsertAsync<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source, int batchSize)
			where TEntity : class
			where TContext : DbContext
		{
			return context.BulkInsertAsync(source, BulkInsertOptions.Create(batchSize: batchSize));
		}
		public static Task BulkInsertAsync<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source, int batchSize, int timeout)
			where TEntity : class
			where TContext : DbContext
		{
			return context.BulkInsertAsync(source, BulkInsertOptions.Create(timeout, batchSize));
		}

		private static void BulkInsert<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source, BulkInsertOptions options)
			where TEntity : class
			where TContext : DbContext
		{
			try
			{
				context.BulkInsertAsync(source, options)
					.Wait();
			}
			catch (AggregateException exception)
			{
				var originalException = exception.InnerExceptions.First();
				ExceptionDispatchInfo.Capture(originalException).Throw();
			}
		}

		private static async Task BulkInsertAsync<TContext, TEntity>(this TContext context, IEnumerable<TEntity> source, BulkInsertOptions options, SqlTransaction transaction = null)
			where TEntity : class
			where TContext : DbContext
		{
			var factory = new ReaderFactory(context);
			var connection = CreateConnection(context);

			using (var reader = factory.CreateReader(source))
			{
				using (var bulkCopy = CreateInseter(connection, options, reader, transaction))
				{
					await bulkCopy.WriteToServerAsync(reader);
					bulkCopy.Close();
				}
			}
		}

		private static SqlBulkCopy CreateInseter<TEntity>(SqlConnection connection, BulkInsertOptions options, MappedDataReader<TEntity> reader, SqlTransaction transaction)
			where TEntity: class
		{
			var bulkCopy = ( transaction == null )
				? new SqlBulkCopy(connection)
				: new SqlBulkCopy(connection, SqlBulkCopyOptions.UseInternalTransaction, transaction);

			bulkCopy.BatchSize = options.BatchSize;
			bulkCopy.BulkCopyTimeout = options.Timeout;
			bulkCopy.NotifyAfter = 0;
			bulkCopy.DestinationTableName = reader.TableName;

			foreach (var index in Enumerable.Range(0, reader.FieldCount))
			{
				var mapping = new SqlBulkCopyColumnMapping(index, reader.PropertyMappings[index].Column);
				bulkCopy.ColumnMappings.Add(mapping);
			}

			return bulkCopy;
		}

		private static SqlConnection CreateConnection<TContext>(TContext context) where TContext : DbContext
		{
			var connection = context.Database.Connection as SqlConnection;

			if (connection == null)
			{
				throw new InvalidOperationException(
					string.Format("Store connection must be the {0}", typeof(SqlConnection)));
			}

			if (connection.State != ConnectionState.Open)
			{
				connection.Open();
			}

			return connection;
		}
	}
}
