﻿using System;

namespace BulkInsert
{
	public class BulkInsertOptions
	{
		public int BatchSize { get; set; }
		public int Timeout { get; set; }

		public static BulkInsertOptions Create(int? timeout = null, int? batchSize = null)
		{
			if (batchSize.HasValue && batchSize.Value <= 0)
			{
				throw new ArgumentException(
					string.Format("Must be positive, specified: {0}", batchSize), "batchSize");
			}
			if (timeout.HasValue && timeout.Value <= 0)
			{
				throw new ArgumentException(
					string.Format("Must be positive, specified: {0}", timeout), "timeout");
			}

			return new BulkInsertOptions
			{
				BatchSize = batchSize ?? 1000,
				Timeout = timeout ?? 360
			};
		}
	}
}
