﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkInsert.Database
{
	public class ApplicationDbContext : DbContext
	{
		public IDbSet<Book> Books { get; set; }

		public IDbSet<Author> Authors { get; set; }

		public ApplicationDbContext() : 
			base("ApplicationDbContext")
		{
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<Book>().ToTable("Books");
			modelBuilder.Entity<Author>().ToTable("Authors");
		}
	}
}
