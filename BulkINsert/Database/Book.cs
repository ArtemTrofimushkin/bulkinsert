﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BulkInsert.Database
{
	public class Book
	{
		[Key]
		[Column("BookId")]
		public Guid Id { get; set; }

		[Column("BookTitle")]
		public string Title { get; set; }

		[Column("BookISBN")]
		public string ISBN { get; set; }

		[Column("BookDate")]
		public DateTime Date { get; set; }
	}
}
