﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BulkInsert.Database
{
	public class Author
	{
		[Key]
		[Column("AuthorId")]
		public Guid Id { get; set; }

		[Column("AuthorName")]
		public string Name { get; set; }

		[Column("AuthorBirthdate")]
		public DateTime Birthdate { get; set; }
	}
}
