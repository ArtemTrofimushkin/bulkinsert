﻿using System.Data.Entity;
using BulkInsert.Helpers;

namespace BulkInsert.Database
{
	public class DatabaseInitializer :
		DropCreateDatabaseAlways<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			this.CreateBooks(context.Set<Book>());
			this.CreateAuthors(context.Set<Author>());

			context.SaveChanges();
		}

		private void CreateBooks(DbSet<Book> set)
		{
			foreach (var book in DataHelper.GenerateBooks(5))
			{
				set.Add(book);
			}
		}

		private void CreateAuthors(DbSet<Author> set)
		{
			foreach (var author in DataHelper.GenerateAuthors(5))
			{
				set.Add(author);
			}
		}
	}
}
