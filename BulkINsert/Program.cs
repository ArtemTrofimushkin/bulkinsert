﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BulkInsert.Database;
using BulkInsert.Helpers;

namespace BulkInsert
{
	using Extensions;
	using Infrastructure.Mappings;
	using Db = System.Data.Entity.Database;

	public class Bar
	{
		public int Prop { get; set; }
	}

	public class Foo
	{
		public Bar Bar { get; set; }
	}

	internal sealed class Program
	{
		private static void Main(string[] args)
		{
			Configure();
			Load();
			Benchmark();
		}

		private static void Configure()
		{
			AppDomain.CurrentDomain.SetData("DataDirectory", Directory.GetCurrentDirectory());
			Db.SetInitializer(new DatabaseInitializer());
		}

		private static void Load()
		{
			using (var context = new ApplicationDbContext())
			{
				var books = context.Books.ToArray();
				var authors = context.Authors.ToArray();
			}
		}

		private static void Benchmark()
		{
			var counts = new int[] { 10, 100, 1000, 10000, 20000, 30000 };

			Insert(counts, "Manual", InsertManual);
			Insert(counts, "Bulk", InsertBulk);
		}

		private static void Insert(IEnumerable<int> counts, string baseName, Action<ApplicationDbContext, IEnumerable<Book>> action)
		{
			foreach(var count in counts)
			{
				Insert(count, string.Format("{0}_{1}", baseName, count), action);
			}
		}
		private static void Insert(int count, string name, Action<ApplicationDbContext, IEnumerable<Book>> action)
		{
			using (var context = new ApplicationDbContext())
			{
				var source = DataHelper.GenerateBooks(count);
				Action actualAction = () => action(context, source);

				using(var monitor = new TimeMonitor(name, actualAction))
				{
					monitor.Run();
				}
			}
		}

		private static void InsertManual(ApplicationDbContext context, IEnumerable<Book> books) 
		{
			var set = context.Books;

			foreach(var book in books)
			{
				set.Add(book);
			}
		}

		private static void InsertBulk(ApplicationDbContext context, IEnumerable<Book> books)
		{
			context.BulkInsert(books);
		}
	}
}
