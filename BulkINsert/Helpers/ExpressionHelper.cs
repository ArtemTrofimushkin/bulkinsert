﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace BulkInsert.Helpers
{
	public static class ExpressionHelper
	{
		public static Func<TEntity, object> PropertyGetter<TEntity>(string path)
		{
			return PropertyGetter<TEntity, object>(path);
		}
		public static Func<TEntity, TProperty> PropertyGetter<TEntity, TProperty>(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				throw new ArgumentNullException("path");
			}

			var targetType = typeof(TEntity);

			var parameter = Expression.Parameter(targetType, "entity");
			var memberAccess = MemberAccess(parameter, targetType, path.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries));
			var cast = ConvertTo<TProperty>(memberAccess);

			return Expression.Lambda<Func<TEntity, TProperty>>(cast, parameter).Compile();
		}

		private static Expression ConvertTo<TProperty>(MemberExpression memberAccess)
		{
			var resultType = typeof(TProperty);
			var accessType = MemberType(memberAccess.Member);

			if (accessType != resultType)
			{
				return Expression.TypeAs(memberAccess, resultType);
			}

			return memberAccess;
		}

		private static MemberExpression MemberAccess(ParameterExpression parameter, Type targetType, string[] partialPath)
		{
			var result = default(MemberExpression);

			foreach (var path in partialPath)
			{
				result = ( result == null )
					? MemberAccess(parameter, targetType, path)
					: MemberAccess(result, MemberType(result.Member), path);
			}

			return result;
		}
		private static MemberExpression MemberAccess(Expression parameter, Type targetType, string propertyName)
		{
			var property = ReflectionHelper.GetInstanceProperties(targetType)[propertyName];
			return Expression.MakeMemberAccess(parameter, property);
		}

		private static Type MemberType(MemberInfo member)
		{
			return ( member is PropertyInfo )
				? ( (PropertyInfo)member ).PropertyType
				: ( (FieldInfo)member ).FieldType;
		}
	}
}
