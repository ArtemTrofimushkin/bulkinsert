﻿using System;
using System.Diagnostics;

namespace BulkInsert.Helpers
{
	public class TimeMonitor : IDisposable
	{
		public TimeMonitor(string name, Action action)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentNullException("name");
			}
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}

			_name = name;
			_action = action;
			_watch = new Stopwatch();
		}

		public void Run()
		{
			Console.WriteLine(
				"Execute action {0} started, Datetime: {1}, Method: {2}", 
					_name, DateTime.Now, _action.Method);

			_watch.Start();
			_action();
		}

		public void Dispose()
		{
			_watch.Stop();

			Console.WriteLine(
				"Execute action {0} completed, Datetime: {1}, Elapsed time: {2}", 
					_name, DateTime.Now, _watch.Elapsed);
		}

		private readonly string _name;
		private readonly Action _action;
		private readonly Stopwatch _watch;
	}
}
