﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BulkInsert.Helpers
{
	using PropertyMap = IReadOnlyDictionary<string, PropertyInfo>;

	public static class ReflectionHelper
	{
		public static PropertyMap GetInstanceProperties<T>()
		{
			return GetInstanceProperties(typeof(T));
		}
		public static PropertyMap GetInstanceProperties(Type targetType)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}

			PropertyMap cachedValue;

			if (!Cache.TryGetValue(targetType, out cachedValue))
			{
				cachedValue = GetInstancePropertiesInternal(targetType);
				Cache.TryAdd(targetType, cachedValue);
			}

			return cachedValue;
		}

		private static PropertyMap GetInstancePropertiesInternal(Type targetType)
		{
			return targetType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
				.ToDictionary(prop => prop.Name);
		}

		private static readonly ConcurrentDictionary<Type, PropertyMap> Cache = new ConcurrentDictionary<Type, PropertyMap>();
	}
}
