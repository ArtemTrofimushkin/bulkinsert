﻿using System;
using System.Collections.Generic;
using System.Linq;
using BulkInsert.Database;

namespace BulkInsert.Helpers
{
	public static class DataHelper
	{
		public static IEnumerable<Book> GenerateBooks(int count)
		{
			return Generate(count, CreateBook);
		}

		public static IEnumerable<Author> GenerateAuthors(int count)
		{
			return Generate(count, CreateAuthor);
		}

		public static IEnumerable<T> Generate<T>(int count, Func<int, T> action)
		{
			if (count <= 0)
			{
				throw new ArgumentException("Must be positive");
			}

			return Enumerable.Range(0, count)
				.Select(action);
		}

		private static Book CreateBook(int i)
		{
			return new Book
			{
				Title = "Book_" + i,
				Date = DateTime.Now,
				ISBN = Guid.NewGuid().ToString("N"),
				Id = Guid.NewGuid()
			};
		}
		private static Author CreateAuthor(int i)
		{
			return new Author
			{
				Name = "Author_" + i,
				Birthdate = DateTime.Now.AddYears(-Random.Next(10, 60)),
				Id = Guid.NewGuid()
			};
		}

		private static readonly Random Random = new Random();
	}
}
