﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkInsert.Infrastructure.Mappings
{
	public class PropertyMapping
	{
		public string Property { get; private set; }

		public string Column { get; private set; }

		public bool IsPrimaryKey { get; private set; }

		public string DataType
		{
			get
			{
				return _type.Name;
			}
		}

		public string DataTypeFull
		{
			get
			{
				return _type.FullName;
			}
		}

		public PropertyMapping(string property, string column, Type type) : 
			this(property, column, type, false)
		{
		}
		public PropertyMapping(string property, string column, Type type, bool primary)
		{
			#region Validate
			if (string.IsNullOrWhiteSpace(property))
			{
				throw new ArgumentNullException("property");
			}
			if (string.IsNullOrWhiteSpace(column))
			{
				throw new ArgumentNullException("column");
			}
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			#endregion

			Property = property;
			Column = column;
			IsPrimaryKey = primary;
			_type = type;
		}

		private readonly Type _type;
	}
}
