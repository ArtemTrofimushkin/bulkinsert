﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Mapping;
using System.Data.Metadata.Edm;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkInsert.Infrastructure.Mappings
{
	internal class ContextMapping
	{
		public Dictionary<Type, TableMapping> TableMappings { get; private set; }

		public ContextMapping(DbContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			_metadata = ( (IObjectContextAdapter)context ).ObjectContext.MetadataWorkspace;
			_objectItems = ( (ObjectItemCollection)_metadata.GetItemCollection(DataSpace.OSpace) );
			_conceptualItems = _metadata.GetItems<EntityType>(DataSpace.SSpace)
				.ToDictionary(x => x.Name);

			var conceptualContainer = _metadata.GetItems<EntityContainer>(DataSpace.CSpace).Single();

			TableMappings = conceptualContainer.BaseEntitySets
				.OfType<EntitySet>()
				.ToDictionary(this.ClrType, this.CreateTableMapping);
		}

		private TableMapping CreateTableMapping (EntitySet set)
		{
			var elementType = set.ElementType;
			var conceptualType = _conceptualItems[elementType.Name];

			return new TableMapping(set.Name, string.Empty, 
				this.CreatePropertyMappigns(elementType, conceptualType));
		}

		private IEnumerable<PropertyMapping> CreatePropertyMappigns(EntityType elementType, EntityType conceptualType)
		{
			for (int i = 0; i < conceptualType.Properties.Count; i++)
			{
				var column = conceptualType.Properties[i];
				var property = elementType.Properties[i];
				var primitiveType = property.TypeUsage.EdmType as PrimitiveType;
				
				if (primitiveType == null)
				{
					throw new NotSupportedException(
						"Navigation properties not supported");
				}

				yield return new PropertyMapping(
					property.Name,
					column.Name,
					primitiveType.ClrEquivalentType,
					conceptualType.KeyMembers.Contains(column));
			}
		}

		private Type ClrType(EntitySet set)
		{
			return this.ClrType(set.ElementType);
		}
		private Type ClrType(EntityTypeBase set)
		{
			return _metadata.GetItems<EntityType>(DataSpace.OSpace)
				.Select(_objectItems.GetClrType)
				.Single(x => string.Equals(x.Name, set.Name, StringComparison.Ordinal));
		}

		private readonly MetadataWorkspace _metadata;
		private readonly ObjectItemCollection _objectItems;
		private readonly IDictionary<string, EntityType> _conceptualItems;
	}
}
