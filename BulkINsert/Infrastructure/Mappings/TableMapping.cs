﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkInsert.Infrastructure.Mappings
{
	internal class TableMapping
	{
		public string TableName
		{
			get
			{
				return string.IsNullOrWhiteSpace(Schema)
					? string.Format("[{0}]", Table)
					: string.Format("[{0}].[{1}]", Schema, Table);
			}
		}

		public string Table { get; set; }

		public string Schema { get; set; }

		public IList<PropertyMapping> PropertyMappings { get; set; }

		public TableMapping(string table, string schema, IEnumerable<PropertyMapping> mappings)
		{
			#region Validate
			if (string.IsNullOrWhiteSpace(table))
			{
				throw new ArgumentNullException("table");
			}
			if (mappings == null)
			{
				throw new ArgumentNullException("mappings");
			}
			#endregion

			Table = table;
			Schema = schema;
			PropertyMappings = mappings as IList<PropertyMapping> ?? mappings.ToArray();
		}
	}
}
