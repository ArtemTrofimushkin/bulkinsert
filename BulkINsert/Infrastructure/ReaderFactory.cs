﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using BulkInsert.Infrastructure.Mappings;

namespace BulkInsert.Infrastructure
{
	public class ReaderFactory
	{
		public ReaderFactory(DbContext context)
		{
			_contextMapping = new ContextMapping(context);
		}

		public MappedDataReader<TEntity> CreateReader<TEntity>(IEnumerable<TEntity> source)
			where TEntity: class
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}

			var mapping = _contextMapping.TableMappings[typeof(TEntity)];
			return new MappedDataReader<TEntity>(mapping.TableName, source, mapping.PropertyMappings);
		}

		private readonly ContextMapping _contextMapping;
	}
}
