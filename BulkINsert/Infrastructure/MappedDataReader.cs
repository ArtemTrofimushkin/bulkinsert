﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BulkInsert.Helpers;
using BulkInsert.Infrastructure.Mappings;

namespace BulkInsert.Infrastructure
{
	public class MappedDataReader<TEntity> : DbDataReader
		where TEntity: class
	{
		public IList<PropertyMapping> PropertyMappings
		{
			get
			{
				return _properties;
			}
		}

		public string TableName { get; private set; }

		public override int FieldCount
		{
			get
			{
				return _properties.Count;
			}
		}

		public override int RecordsAffected
		{
			get
			{
				return _sourceCollection.Count;
			}
		}

		public override bool HasRows
		{
			get
			{
				return _sourceCollection != null && _sourceCollection.Count > 0;
			}
		}

		public override bool IsClosed
		{
			get
			{
				return _enumerator == null;
			}
		}

		public override object this[int ordinal]
		{
			get
			{
				return this.GetValue(ordinal);
			}
		}

		public override object this[string name]
		{
			get
			{
				var ordinal = this.GetOrdinal(name);
				return this.GetValue(ordinal);
			}
		}

		public MappedDataReader(
			string tableName,
			IEnumerable<TEntity> source, 
			IEnumerable<PropertyMapping> mappings)
		{
			#region Validate
			if (string.IsNullOrWhiteSpace(tableName))
			{
				throw new ArgumentNullException("tableName");
			}
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (mappings == null)
			{
				throw new ArgumentNullException("mappings");
			}
			#endregion

			TableName = tableName;

			_sourceCollection = source.ToArray();
			_enumerator = source.GetEnumerator();
			_properties = mappings.ToArray();
			_accessors = _properties.Select(map => ExpressionHelper.PropertyGetter<TEntity>(map.Property)).ToArray();
		}

		public override void Close()
		{
			base.Close();

			if (IsClosed)
			{
				return;
			}

			_enumerator.Dispose();
			_enumerator = null;
			_sourceCollection = null;
		}

		public override bool Read()
		{
			this.EnsureNotClosed();
			return _enumerator.MoveNext();
		}

		public override IEnumerator GetEnumerator()
		{
			this.EnsureNotClosed();
			return _enumerator;
		}

		public override bool IsDBNull(int ordinal)
		{
			this.EnsureNotClosed();
			return _enumerator.Current == null;
		}

		public override object GetValue(int ordinal)
		{
			this.EnsureNotClosed();
			return _accessors[ordinal](_enumerator.Current);
		}

		public override int GetOrdinal(string name)
		{
			this.EnsureNotClosed();

			var item = _properties.First(
				p => string.Equals(p.Property, name, StringComparison.Ordinal));

			return _properties.IndexOf(item);
		}

		#region NotUsed
		public override int Depth
		{
			get
			{
				throw new NotSupportedException();
			}
		}
		public override bool GetBoolean(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override byte GetByte(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override char GetChar(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
		{
			throw new NotSupportedException();
		}
		public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
		{
			throw new NotSupportedException();
		}
		public override short GetInt16(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override int GetInt32(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override long GetInt64(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override float GetFloat(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override double GetDouble(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override decimal GetDecimal(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override Guid GetGuid(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override DateTime GetDateTime(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override string GetString(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override string GetName(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override string GetDataTypeName(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override Type GetFieldType(int ordinal)
		{
			throw new NotSupportedException();
		}
		public override int GetValues(object[] values)
		{
			throw new NotSupportedException();
		}
		public override bool NextResult()
		{
			throw new NotSupportedException();
		}
		#endregion

		private void EnsureNotClosed()
		{
			if (IsClosed)
			{
				throw new InvalidOperationException("Reader is closed");
			}
		}

		private IEnumerator<TEntity> _enumerator;
		private IList<TEntity> _sourceCollection;
		private IList<PropertyMapping> _properties;
		private readonly IList<Func<TEntity, object>> _accessors;
	}
}
